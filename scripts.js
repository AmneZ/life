const canvas = document.getElementById('gameField');
const canvasContext = canvas.getContext('2d');

const gridCanvas = document.getElementById('gameGrid');
const gridCanvasContext = gridCanvas.getContext('2d');

const fieldSizeInput = document.getElementById('fieldSizeInput');
const cellSizeInput = document.getElementById('cellSizeInput');

const startButton = document.getElementById('start');
const stopButton = document.getElementById('stop');

let CANVAS_SIZE = 300;
let CELL_SIZE = 10;
let FIELD_SIZE = CANVAS_SIZE / CELL_SIZE;

fieldSizeInput.value = CANVAS_SIZE;
cellSizeInput.value = CELL_SIZE;

const STROKE_COLOR = '#7d7d7d';

let fieldDataArray = new Array(FIELD_SIZE);
for (let i = 0; i < fieldDataArray.length; i++) {
    fieldDataArray[i] = new Array(FIELD_SIZE);
}

let fieldDataMap = new Map();

let isGameStartedControl = false;

canvas.onclick = ({offsetX, offsetY}) => {
    if(isGameStartedControl) return;
    const xIndex = Math.floor(offsetX / CELL_SIZE);
    const yIndex = Math.floor(offsetY / CELL_SIZE);
    const key = `${xIndex}:${yIndex}`;
    if(fieldDataMap.get(key)) {
        clearCanvas(fieldDataMap.get(key));
        fieldDataMap.delete(key)
    } else {
        fieldDataMap.set(key, {yIndex, xIndex});
    }
    redrawField();
}

const drawCellsGrid = () => {
    gridCanvasContext.clearRect(0,0, gridCanvas.width, gridCanvas.height);
    if(CELL_SIZE < 3) return;
    gridCanvasContext.beginPath();
    for (let i = 0; i < FIELD_SIZE; i++) {
        gridCanvasContext.moveTo(i * CELL_SIZE, 0);
        gridCanvasContext.lineTo(i * CELL_SIZE, canvas.height);

        gridCanvasContext.moveTo(0, i * CELL_SIZE);
        gridCanvasContext.lineTo(canvas.width, i * CELL_SIZE);
    }
    gridCanvasContext.closePath();
    gridCanvasContext.strokeStyle = STROKE_COLOR;
    gridCanvasContext.lineWidth = 1;
    gridCanvasContext.stroke();
}

const clearCanvas = (specificCoordinates) => {
    if(specificCoordinates) {
        canvasContext.clearRect(specificCoordinates.xIndex * CELL_SIZE, specificCoordinates.yIndex * CELL_SIZE , CELL_SIZE, CELL_SIZE);
    } else {
        const mapIterator = fieldDataMap.values();
        let currentValue = mapIterator.next().value;
        while (currentValue) {
            canvasContext.clearRect(currentValue.xIndex * CELL_SIZE, currentValue.yIndex * CELL_SIZE , CELL_SIZE, CELL_SIZE);
            currentValue = mapIterator.next().value;
        }
    }
}

const redrawField = () => {
    const mapIterator = fieldDataMap.values();
    let currentValue = mapIterator.next().value;
    while (currentValue) {
        canvasContext.fillRect(currentValue.xIndex * CELL_SIZE, currentValue.yIndex * CELL_SIZE , CELL_SIZE, CELL_SIZE);
        currentValue = mapIterator.next().value;
    }
}

const calculateOutOfBoundsCoordinate = (coordinate) => {
    if (coordinate < 0) return FIELD_SIZE - 1;
    if (coordinate >= FIELD_SIZE) return 0;
    return coordinate;
}

const getNeighboursOfCellUpdated = (xIndex, yIndex) => {
    let neighboursCount = 0;
    if (fieldDataMap.get(`${calculateOutOfBoundsCoordinate(xIndex - 1)}:${yIndex}`)) neighboursCount++;
    if (fieldDataMap.get(`${xIndex}:${calculateOutOfBoundsCoordinate(yIndex + 1)}`)) neighboursCount++;
    if (fieldDataMap.get(`${calculateOutOfBoundsCoordinate(xIndex + 1)}:${yIndex}`)) neighboursCount++;
    if (fieldDataMap.get(`${xIndex}:${calculateOutOfBoundsCoordinate(yIndex - 1)}`)) neighboursCount++;
    if (fieldDataMap.get(`${calculateOutOfBoundsCoordinate(xIndex - 1)}:${calculateOutOfBoundsCoordinate(yIndex + 1)}`)) neighboursCount++;
    if (fieldDataMap.get(`${calculateOutOfBoundsCoordinate(xIndex + 1)}:${calculateOutOfBoundsCoordinate(yIndex + 1)}`)) neighboursCount++;
    if (fieldDataMap.get(`${calculateOutOfBoundsCoordinate(xIndex + 1)}:${calculateOutOfBoundsCoordinate(yIndex - 1)}`)) neighboursCount++;
    if (fieldDataMap.get(`${calculateOutOfBoundsCoordinate(xIndex - 1)}:${calculateOutOfBoundsCoordinate(yIndex - 1)}`)) neighboursCount++;
    return neighboursCount;
}

const normalizeCoordinate = (coordinate) => {
    if(coordinate < 0) return FIELD_SIZE - 1;
    if(coordinate > FIELD_SIZE - 1) return 0;
    return coordinate;
}

const calculateNeighboursStates = (xIndex, yIndex, updatedFieldDataMap) => {
    const normalizedX = normalizeCoordinate(xIndex), normalizedY = normalizeCoordinate(yIndex);

    if (updatedFieldDataMap.has(`${normalizedX}:${normalizedY}`)) return;

    const neighboursCount = getNeighboursOfCellUpdated(normalizedX, normalizedY);

    if(neighboursCount === 3 && neighboursCount !== 2) {
        updatedFieldDataMap.set(`${normalizedX}:${normalizedY}`, {xIndex: normalizedX, yIndex: normalizedY})
    }

    const existingValue = fieldDataMap.get(`${normalizedX}:${normalizedY}`);
    if((neighboursCount === 3 || neighboursCount === 2) && existingValue) {
        updatedFieldDataMap.set(`${normalizedX}:${normalizedY}`, existingValue);
    }

}

const evolve = () => {
    if(stopControlFlag) {
        stopControlFlag = false;
        clearCanvas();
        fieldDataMap = new Map();
        return;
    }
    isGameStartedControl = true;
    const updatedFieldDataMap = new Map();

    const mapIterator = fieldDataMap.values();
    let currentValue = 1;

    while (currentValue) {
        currentValue = mapIterator.next().value;
        if(!currentValue) continue;

        const {xIndex, yIndex} = currentValue;
        const neighboursCount = getNeighboursOfCellUpdated(xIndex, yIndex);

        calculateNeighboursStates(xIndex - 1, yIndex - 1, updatedFieldDataMap);
        calculateNeighboursStates(xIndex - 1, yIndex, updatedFieldDataMap);
        calculateNeighboursStates(xIndex, yIndex - 1, updatedFieldDataMap);
        calculateNeighboursStates(xIndex + 1, yIndex - 1, updatedFieldDataMap);
        calculateNeighboursStates(xIndex + 1, yIndex, updatedFieldDataMap);
        calculateNeighboursStates(xIndex + 1, yIndex + 1, updatedFieldDataMap);
        calculateNeighboursStates(xIndex, yIndex + 1, updatedFieldDataMap);
        calculateNeighboursStates(xIndex - 1, yIndex + 1, updatedFieldDataMap);

        const existingValue = fieldDataMap.get(`${xIndex}:${yIndex}`);
        if((neighboursCount === 3 || neighboursCount === 2) && existingValue) {
            updatedFieldDataMap.set(`${xIndex}:${yIndex}`, existingValue);
        }
    }

    clearCanvas();
    fieldDataMap = new Map(updatedFieldDataMap);
    redrawField();
    requestAnimationFrame(evolve);
}

drawCellsGrid();

const recalculateFieldSize = () => FIELD_SIZE = CANVAS_SIZE / CELL_SIZE;

const updateCanvasSize = () => {
    canvas.width = CANVAS_SIZE;
    canvas.height = CANVAS_SIZE;

    gridCanvas.width = CANVAS_SIZE;
    gridCanvas.height = CANVAS_SIZE;

    drawCellsGrid();
}

fieldSizeInput.onchange = ({target: {value}}) => {
    if(value < 100) startButton.disabled = true;
    else {
        startButton.disabled = false;
        CANVAS_SIZE = +value;
        recalculateFieldSize();
        updateCanvasSize();
    }
};

cellSizeInput.onchange = ({target: {value}}) => {
    if(CANVAS_SIZE / value < 4) startButton.disabled = true;
    else {
        startButton.disabled = false;
        CELL_SIZE = +value;
        recalculateFieldSize();
        drawCellsGrid();
    }
};

startButton.onclick = () => {
    startButton.disabled = true;
    stopButton.disabled = false;
    fieldSizeInput.disabled = true;
    cellSizeInput.disabled = true;
    evolve();
}

stopButton.disabled = true;

let stopControlFlag = false;
stopButton.onclick = () => {
    isGameStartedControl = false;
    stopControlFlag = true;
    stopButton.disabled = true;
    startButton.disabled = false;
    fieldSizeInput.disabled = false;
    cellSizeInput.disabled = false;
}

redrawField();
